const argv = require('yargs')
  .option('b', {
    alias: 'base',
    type: 'number',
    demandOption: true,
    describe: 'Es la base de la tabla multiplicar',
  })
  .option('h', {
    alias: 'hasta',
    type: 'number',
    demandOption: true,
    describe: 'Valor hasta donde multiplicar',
  })
  .option('l', {
    alias: 'listar',
    type: 'boolean',
    default: false,
    describe: 'Muestra la tabla en consola',
  })
  .check((argv, option) => {
    if (isNaN(argv.base)) {
      throw 'La base debe ser un número';
    }
    return true;
  }).argv;

module.exports = {
  argv,
};

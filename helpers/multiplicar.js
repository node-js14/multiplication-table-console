const fs = require('fs');
require('colors');

const crearArchivo = async (base = 5, listar = false, hasta = 10) => {
  try {
    let salida = '';

    for (let item = 1; item <= hasta; item++) {
      salida += ` ${base} * ${item} = ${base * item} \n`;
    }

    if (listar) {
      console.log('==========================='.green);
      console.log(`====== TABLA DEL ${base} =======`.yellow);
      console.log('==========================='.green);
      console.log(salida.cyan);
    }

    fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);

    return `tabla-${base}.txt `;
  } catch (err) {
    throw new Error();
  }
};

module.exports = { crearArchivo };
